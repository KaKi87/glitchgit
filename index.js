const
    fs = require('fs'),
    fastify = require('fastify')(),
    { default: PQueue } = require('p-queue');

const { key, sentry, gitea, github } = require('./lib');

{
    const config = JSON.parse(fs.readFileSync('config.json', 'UTF-8'));
    config.projects.forEach((project, index) => {
        if(project.key) return;
        project.key = key();
        console.log(`Key generated for project ID #${index + 1}`);
    });
    fs.writeFileSync('config.json', JSON.stringify(config, null, 4), 'UTF-8');
}

const { port, projects, accounts } = require('./config');

const queues = {};

fastify.register(require('fastify-cors'));

fastify.get('/', async (request, reply) => reply.code(204).send());

fastify.post('/api/:projectId/store/', async (request, reply) => {
    const
        { projectId } = request.params,
        projectKey = request.query['sentry_key'] || request.headers['x-sentry-auth'].slice(-32);
    const
        project = projects[projectId - 1],
        account = accounts[project.account - 1];
    if(!project || !project.key || project.key !== projectKey || !account)
        return reply.code(404).send();
    const
        { repository } = project,
        { type, token, host } = account,
        queue = queues[repository] ? queues[repository] : queues[repository] = new PQueue({ concurrency: 1 });
    let payload, res;
    try {
        if(typeof request.body === 'string')
            payload = JSON.parse(request.body);
        else if(typeof request.body === 'object')
            payload = request.body;
    } catch(_){}
    if(!payload)
        return reply.code(400).send();
    switch(type){
        case 'gitea': {
            res = sentry(payload, await gitea.getSourcePath(token, host, repository));
            await queue.add(async () => await gitea(token, host, repository, res));
            break;
        }
        case 'github': {
            res = sentry(payload, await github.getSourcePath(token, repository));
            await queue.add(async () => await github(token, repository, res));
            break;
        }
        default: reply.code(400).send('accounts[].type must be one of : "gitea", "github"');
    }
    reply.send({ id: res.eventId });
});

fastify.listen({
    port,
    host: '0.0.0.0',
    maxParamLength: 200
}).then(() => console.log('Server running')).catch(console.error);