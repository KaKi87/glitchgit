module.exports = {
    key: require('./key'),
    sentry: require('./sentry'),
    gitea: require('./gitea'),
    github: require('./github')
};