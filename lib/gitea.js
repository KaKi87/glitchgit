const axios = require('axios');

const { ignoredIssuesLabel } = require('../config');

module.exports = async (
    token,
    host = 'https://try.gitea.io',
    repository,
    { message, title, body }
) => {
    const
        issuesEndpoint = `${host}/api/v1/repos/${repository}/issues`,
        params = { 'access_token': token };
    const { data } = await axios(issuesEndpoint, {
        params: {
            ...params,
            q: message,
            state: 'all'
        }
    });
    const originalIssue = data.find(issue => issue.title.endsWith(message));
    if(originalIssue){
        if(Array.isArray(originalIssue['labels']) && originalIssue['labels'].find(label => label['name'] === (ignoredIssuesLabel || 'wontfix')))
            return;
        if(originalIssue['closed_at']){
            await axios.patch(`${issuesEndpoint}/${originalIssue['number']}`, {
                state: 'open'
            }, { params });
        }
        await axios.post(`${issuesEndpoint}/${originalIssue['number']}/comments`, {
            body: `${title}\n${body}`
        }, { params });
    }
    else {
        await axios.post(issuesEndpoint, {
            title,
            body
        }, { params });
    }
};

module.exports.getSourcePath = async (token, host, repository) => {
    const { data } = await axios(`${host}/api/v1/repos/${repository}`, { params: { 'access_token': token } });
    return `${host}/${repository}/src/branch/${data['default_branch']}`;
};