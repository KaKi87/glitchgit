const axios = require('axios');

const { ignoredIssuesLabel } = require('../config');

const host = 'https://api.github.com';

module.exports = async (
    token,
    repository,
    { message, title, body }
) => {
    const
        issuesEndpoint = `${host}/repos/${repository}/issues`,
        headers = { 'Authorization': `token ${token}` };
    const { data } = await axios(`${host}/search/issues`, {
        params: {
            q: `type:issue ${message} in:title repo:${repository}`
        },
        headers
    });
    const originalIssue = Array.isArray(data.items) && data.items.find(issue => issue.title.endsWith(message));
    if(originalIssue){
        if(Array.isArray(originalIssue['labels']) && originalIssue['labels'].find(label => label['name'] === (ignoredIssuesLabel || 'wontfix')))
            return;
        if(originalIssue['closed_at']){
            await axios.patch(`${issuesEndpoint}/${originalIssue['number']}`, {
                state: 'open'
            }, { headers });
        }
        await axios.post(`${issuesEndpoint}/${originalIssue['number']}/comments`, {
            body: `${title}\n${body}`
        }, { headers });
    }
    else {
        await axios.post(issuesEndpoint, {
            title,
            body
        }, { headers });
    }
};

module.exports.getSourcePath = async (token, repository) => {
    const { data } = await axios(`${host}/repos/${repository}`, { headers: { 'Authorization': `token ${token}` } });
    return `https://github.com/${repository}/blob/${data['default_branch']}`;
};