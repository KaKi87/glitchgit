const { name, repository, version } = require('../package');

const { maxBreadcrumbsCount } = require('../config');

const crypto = require('crypto');

const outdent = require('outdent');

const breadcrumbCategoryEmoji = {
    'app.lifecycle'      : ':recycle:',
    'attachment'         : ':floppy_disk:',
    'auth'               : ':passport_control:',
    'click'              : ':computer_mouse:',
    'crash'              : ':exclamation:',
    'console'            : ':memo:',
    'debug'              : ':bug:',
    'default'            : ':date:',
    'device.event'       : ':date:',
    'device.orientation' : ':compass:',
    'dom'                : ':page_facing_up:',
    'error'              : ':exclamation:',
    'exception'          : ':exclamation:',
    'fetch'              : ':globe_with_meridians:',
    'generic'            : ':date:',
    'history'            : ':motorway:',
    'http'               : ':globe_with_meridians:',
    'httplib'            : ':globe_with_meridians:',
    'info'               : ':information_source:',
    'log'                : ':memo:',
    'navigation'         : ':motorway:',
    'query'              : ':mag_right:',
    'redis'              : ':card_file_box:',
    'request'            : ':globe_with_meridians:',
    'security'           : ':lock:',
    'sentry.event'       : ':date:',
    'session'            : ':bust_in_silhouette:',
    'ui.click'           : ':computer_mouse:',
    'ui.input'           : ':keyboard:',
    'ui.lifecycle'       : ':recycle:',
    'user'               : ':bust_in_silhouette:',
    'xhr'                : ':globe_with_meridians:'
};

module.exports = (payload, sourcePath) => {
    const
        eventId = payload['event_id'],
        message = (payload['message']
            || payload['exception']['values'][0]['value']
            || `Unknown error ${crypto.createHash('md5').update(JSON.stringify(payload['exception']['values'][0])).digest('hex').slice(0, 7)}`).trim();
    return {
        eventId,
        message,
        title: `[Sentry #${eventId.slice(0, 7)}] ${message}`,
        body: outdent
        `
            # ${payload['message'] ? ':speech_balloon:' : ':x:'} ${message}
            
            ${payload['exception'] ? outdent `
                ## Exception
                > ${payload['exception']['values'][0]['value']}
                ${payload['exception']['values'][0]['stacktrace']['frames'].map(frame => {
                    const pathItems = frame['filename'].split('/');
                    const moduleFileIndex = pathItems.findIndex(item => item.startsWith(frame['module']));
                    const isProject = frame['in_app'];
                    const dependencyDirectoryIndex = pathItems.indexOf('node_modules');
                    const isDependency = dependencyDirectoryIndex !== -1;
                    const isNative = !isProject && !isDependency;
                    let path, url;
                    if(isProject){
                        path = pathItems.slice(moduleFileIndex - 1).join('/');
                        url = `${sourcePath}/${pathItems.slice(moduleFileIndex).join('/')}#L${frame['lineno']}`;
                    }
                    if(isDependency){
                        path = pathItems.slice(dependencyDirectoryIndex + 1).join('/');
                        url = 'https://unpkg.com/' + path;
                    }
                    if(isNative){
                        path = pathItems.join('/');
                    }
                    path = path.replace(/_/g, '\\_');
                    return `- ${frame['in_app'] ? '' : '_'}at \`${frame['function']}\` @ ${url ? `[${path}](${url})` : path}:​${frame['lineno']}:${frame['colno']}${frame['in_app'] ? '' : '_'}`;
                }).join('\n')}
            ` : ''}
            
            ${payload['breadcrumbs'] ? outdent `
                ## Breadcrumbs
                <details>
                <summary>Click to expand</summary>
                
                | Type  | Date  | Level | Data |
                | :---: | :---: | :---: | ---- |
                ${payload['breadcrumbs'].sort((breadcrumb1, breadcrumb2) => breadcrumb2['timestamp'] - breadcrumb1['timestamp']).slice(0, maxBreadcrumbsCount || 25).map(breadcrumb => outdent `
                    | ${breadcrumbCategoryEmoji[breadcrumb['category']] || ''}<br>\`${breadcrumb['category']}\` | \`${new Date(breadcrumb['timestamp'] * 1000).toISOString()}\` | ${breadcrumb['level'] ? `\`${breadcrumb['level']}\`` : '-'} | \`${(breadcrumb['message'] || JSON.stringify(breadcrumb['data'], null, 4)).split('\n').filter(l => !!l).join('\`<br>\`')}\` |
                `).join('\n')}
                </details>
                
                <br>
            ` : ''}
            
            ${payload['contexts'] ? outdent `
                ## Contexts
                \`\`\`json
                ${JSON.stringify(payload['contexts'], null, 4)}
                \`\`\`
            ` : ''}
            
            ${payload['extra'] ? outdent `
                ## Extra
                \`\`\`json
                ${JSON.stringify(payload['extra'], null, 4)}
                \`\`\`
            ` : ''}
            
            ${payload['request'] ? outdent `
                ## Request
                URL : [${payload['request']['url']}](${payload['request']['url']})
                
                ${payload['request']['headers'] ? outdent `
                    | Headers    | (${Object.keys(payload['request']['headers']).length}) |
                    | -------    | :----------------------------------------------------- |
                    ${Object.entries(payload['request']['headers']).map(([key, value]) => outdent `
                    | \`${key}\` | \`${value}\`                                           |
                    `)}
                ` : ''}
            ` : ''}
            
            ## Meta
            
            | [\`${name}\`](${repository}) |  \`${version}\`                                                  |
            | ---------------------------- | :--------------------------------------------------------------- |
            | Level                        | \`${payload['level']}\`                                          |
            | Event ID                     | \`${payload['event_id']}\`                                       |
            | Platform                     | \`${payload['platform']}\`                                       |
            | SDK                          | \`${payload['sdk']['name']}\`, \`${payload['sdk']['version']}\`  |
            | Date                         | \`${new Date(payload['timestamp'] * 1000).toISOString()}\` |
        `
    };
};