# glitchgit

[![](https://shields.kaki87.net/endpoint?url=https%3A%2F%2Fraw.githubusercontent.com%2Fserver-KaTys%2Fstatus%2Fmaster%2Fapi%2Fglitchgit-ka-ki87%2Fuptime.json)](https://status.katys.cf/history/glitchgit-ka-ki87)

Error tracking back-end generating git repository issues compatible with the Sentry SDK.

Idea and name inspired by [GlitchTip](https://glitchtip.com/), an alternative error tracking application compatible with the Sentry SDK.

Compatible git platforms :
- [Gitea](https://gitea.io/en-us/)
- [GitHub](https://github.com/)

## Getting started

1. Get git platform account tokens

- Get a Gitea account token : `/user/settings/applications`
- [Get a GitHub account token](https://github.com/settings/tokens/new) (`public_repo` scope required for public repos, `repo` scope required for private repos)

2. Clone the project, install its dependencies, create the configuration file

```bash
git clone https://git.kaki87.net/KaKi87/glitchgit.git
cd glitchgit
yarn install
cp config.example.json config.json
```

3. Update the configuration file

- `port` *(integer)* : web server port, to be used in Sentry DSN or virtual host
- `accounts` *(array)* : git platform accounts
  - `accounts[].type` *(string)* : git platform type, must be `gitea` or `github`
  - `accounts[].token` *(string)* : git platform account token
  - `accounts[].host` *(string)* : git platform host
- `projects` *(array)* : Sentry projects
  - `projects[].account` *(integer)* : `accounts[]` index (starts at 1)
  - `projects[].repository` *(string)* : git platform repository name in `user/repo` or `org/repo` format
- `maxBreadcrumbsCount` *(integer)* : max [Sentry breadcrumbs](https://sentry.io/features/breadcrumbs/) count
- `ignoredIssuesLabel` *(string)* : git platform issue label to ignore further reports

3. Start the server

```bash
yarn start
```

4. Open `config.json` to get auto-generated 32-chars project IDs to be used in Sentry DSNs

Sentry DSN format : `protocol://key@host/index`

- `protocol` = `http` or `https`
- `key` = `projects[].key`
- `index` = `projects[]` index (starts at 1)